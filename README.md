# About Beitify
Welcome to Beitify, your CHAIRway to heaven! Beitify is where you can FINALLY schedule an appointment to try the chair of your DREAMS. Built with Angular, responsive, and above all, AWESOME:)!

![alt text](https://i.ibb.co/51V36Gt/f1.png) ![alt text](https://i.ibb.co/R3BhJsX/f22.png) ![alt text](https://i.ibb.co/9ydjj7P/wohoo.png) 



# Beitify

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
