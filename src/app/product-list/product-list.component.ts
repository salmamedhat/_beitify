import { Component, OnInit, Inject } from "@angular/core";

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { DialogData } from "../DialogData";
import { ModalComponent } from "../modal/modal.component";
import { ChairsService } from "../chairs.service";
import { HttpClientModule, HttpClient } from "@angular/common/http";

@Component({
  selector: "app-product-list",
  templateUrl: "./product-list.component.html",
  styleUrls: ["./product-list.component.css"]
})
export class ProductListComponent implements OnInit {
  mobileNumber: string;
  date: Date;

  albums;
  chairss: any[];
  arr;

  constructor(
    public dialog: MatDialog,
    private chairsService: ChairsService,
    http: HttpClient
  ) {
    http
      .get("https://salmamedhat.github.io/data/db.json")
      .subscribe(response => {
        console.log(response);
        this.arr = response;
        console.log("After assigning: " + this.arr);
      });
  }

  openDialog(chair: any): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: "75%",
      height: "80%",

      data: {
        date: this.date,
        mobileNumber: this.mobileNumber,
        img: chair.img,
        title: chair.title,
        subtitle: chair.subtitle
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

  ngOnInit(albums = this.chairsService.getAlbums()) {}

  // pictures = [
  //   {
  //     id: 1,
  //     title: "Jex Jaxon",
  //     img:
  //       "https://i.ibb.co/1z1BbKJ/1.jpg",
  //     subtitle:
  //       "One of the best chairs ever created. Truly a masterpiece. Stood the test of time.",
  //     content: "conn"
  //   },
  //   {
  //     id: 2,
  //     title: "Twist & Shout",
  //     img:
  //       "https://i.ibb.co/wpZb2r6/2.jpg",
  //     subtitle:
  //       "A basic yet fancy stool with adjustable height. Simply spin the seat and adjust your needs.",
  //     content: "conn"
  //   },
  //   {
  //     id: 3,
  //     title: "Wooden You Know",
  //     img:
  //       "https://i.ibb.co/Lryr2G6/3.jpg",
  //     subtitle:
  //       "Contemporary, modern and elegant. This wooden creation is a treat for your seat.",
  //     content: "conn"
  //   },
  //   {
  //     id: 4,
  //     title: "Loners Dream",
  //     img:
  //       "https://i.ibb.co/H7z3txR/4.jpg",
  //     subtitle:
  //       "We only made one of these. Going for a staggering $20,000, all proceeds will go to charity.",
  //     content: "conn"
  //   }
  // ];
}
