import { Component,ViewEncapsulation, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class AppComponent  {
  title = 'Beitify';
  constructor(private renderer: Renderer2) {
    this.renderer.addClass(document.body, 'body-class');
    }
}
