import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule, MatSidenavModule,MatToolbarModule,MatIconModule, MatButtonModule,MatListModule   } from '@angular/material';


import {DragDropModule} from '@angular/cdk/drag-drop';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';


import { MatDialogModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';


import { MatDatepickerModule,
        MatNativeDateModule,
        
         } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatTabsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,

    MatButtonModule,
    
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    MatTabsModule,
    MatToolbarModule,

    FormsModule, MatDialogModule, MatFormFieldModule, MatButtonModule, MatInputModule,

    MatDatepickerModule,

    MatNativeDateModule,
    BrowserAnimationsModule


  ],
  exports: [
    MatTabsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule ,
    MatListModule,

    MatButtonModule,
    
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    MatTabsModule,
    MatToolbarModule,

    FormsModule, MatDialogModule, MatFormFieldModule, MatButtonModule, MatInputModule,
    MatDatepickerModule,

    MatNativeDateModule,
    BrowserAnimationsModule
  ],

  providers: [ MatDatepickerModule ],


})
export class MaterialModule { }
