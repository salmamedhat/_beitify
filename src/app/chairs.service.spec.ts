import { TestBed } from '@angular/core/testing';

import { ChairsService } from './chairs.service';

describe('ChairsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChairsService = TestBed.get(ChairsService);
    expect(service).toBeTruthy();
  });
});
