import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { DialogData } from "../DialogData";
import Swal from "sweetalert2";

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.css"]
})
export class ModalComponent implements OnInit {
  theDate;
  theNumber;
  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {}

  confirm() {
    console.log(this.theDate);
    if (!this.theDate || !this.theNumber) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Please fill in all information, so we can schedule your visit."
      });
    } else {
      Swal.fire({
        icon: 'success',
        title: 'Visit booked successfully.',
        showConfirmButton: false,
        timer: 2000,
        backdrop: `
          rgba(44,180,116,0.4)
          url("https://i.gifer.com/6ob.gif")
          left top
          repeat
        `
      })
    }
  }
}
